package com.company.StreamApiTask;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.Function;

public class Main {

    private static volatile Integer result = 0;

    public static void main(String[] args) {
	// write your code here

        //Dop псле того, как были пропарсены все файлы, программа запскается заного, парсин все и пишет изменения

        String path1 = "/home/xw/IdeaProjects/StreamApiTask/TestFiles/text1.txt";
        String path2 = "/home/xw/IdeaProjects/StreamApiTask/TestFiles/test2.txt";

        String[] resources = {path1, path2};

        Calculator calculator = (String s) ->{
            Thread thread = new Thread(new Runnable() {
                Integer r = new Integer(0);
                @Override
                public void run() {
                    String line;
                    System.out.println("Thread for file " + s + " start");
                    try {
                        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(s)));
                        try {
                            while ((line = in.readLine()) != null){
                                String[] splitted = line.split(" ");
                                for (int i = 0; i < splitted.length; i++){
                                    System.out.println("parse value " + Integer.parseInt(splitted[i]));
                                    Main.result = Main.result + Integer.parseInt(splitted[i]);
                                    System.out.println(Main.result);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
            return thread;
        };


        Arrays.stream(resources).parallel().map(s -> {
            calculator.calcSum(s).start();
            return 0;
        }).toArray();


    }
}
